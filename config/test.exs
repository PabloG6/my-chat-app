use Mix.Config

# Configure your database
config :my_chat_app, MyChatApp.Repo,
  username: "postgres",
  password: "postgres",
  database: "my_chat_app_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :bcrypt_elixir, :log_rounds, 4
# We don't run a server during test. If one is required,
# you can enable the server option below.
config :my_chat_app, MyChatAppWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
